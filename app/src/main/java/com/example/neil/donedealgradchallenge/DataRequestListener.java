package com.example.neil.donedealgradchallenge;

import android.graphics.Bitmap;

import com.example.neil.donedealgradchallenge.model.simple.Model;

/**
 * Created by Neil on 24/05/2017.
 *
 * Listener to return the json data (as a model) and/or the images to the calling class.
 *
 */

public interface DataRequestListener {

    /*
        OnDataReady: when data is available this will return it to the invoking class and flags to
                     confirm it came through ok.
        @param:
            pass: boolean to tell us if the request suceeded
            http_code: The code of the request to again see if it suceeded or failed
            model: the data model for the data.

     */

    void onDataReady(boolean pass, String http_code, Model model);

    /*
        imageReady: when the image is complete this will inform the invoking class allowing it to
                    process it.
            @Param:
                bitmap: the now ready to use bitmap.
     */

    void imageReady(Bitmap bitmap);


}
