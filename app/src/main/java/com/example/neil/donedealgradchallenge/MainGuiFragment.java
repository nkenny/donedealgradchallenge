package com.example.neil.donedealgradchallenge;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.neil.donedealgradchallenge.model.simple.Model;


/**
 * Created by Neil on 27/05/2017.
 *
 *  MainGuiFragment: A fragment that displays the data.
 *
 */

public class MainGuiFragment extends Fragment {


    private boolean modelPassedFromFragment = false;

    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvAbv;
    private TextView tvCategoryName;
    private ImageView imageView;
    private ProgressBar progressBar;


    private int screenWidth;
    private int screenHeight;
    private DataRequest dataRequest;
    private PassModel passModel;
    private ImageHandler imageHandler;


    private Bundle bundle;

    /*
        onCreateView:   Creates the view, instantiates and has the previously created model
                        passed to it if applicable.

     */

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        bundle = getArguments();

        if(bundle != null) {
            modelPassedFromFragment = true;
        }

        return inflater.inflate(R.layout.main_gui, container, false);
    }

    /*

        onViewCreated:  Responsible for the instantiation of the items in the layout, getting the
                        screen size and setting the items data if it has been passed in otherwise
                        it calls retrieve data to get that data.

     */

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtainScreenSize();

        tvTitle = (TextView) view.findViewById(R.id.beer_title);
        tvAbv = (TextView) view.findViewById(R.id.abv);
        tvCategoryName = (TextView) view.findViewById(R.id.category_name);
        tvDescription = (TextView) view.findViewById(R.id.description);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        progressBar = (ProgressBar) view.findViewById(R.id.imageViewProgressBar);

        if (modelPassedFromFragment) {

            tvTitle.setText(bundle.getString("name"));
            tvAbv.setText(bundle.getString("abv"));
            tvCategoryName.setText(bundle.getString("category_name"));
            tvDescription.setText(bundle.getString("description"));
            imageView.setImageBitmap(null);
            progressBar.setVisibility(View.VISIBLE);


            Log.d(this.getClass().toString(), "imageHandler called");
            imageHandler  = new ImageHandler(requestListener,bundle.getString("label"),screenWidth, screenHeight );

        } else {
            retrieveData();
        }
    }

    /*
        PassModel: Responsible for passing the created model to the activity for storage.

     */

    public interface PassModel {
        void passModel(Model model);
    }

    /*
       onStop: This was an attempt to stop the thread responsible for getting and scaling the images when
        the fragment was destroyed. It doesn't seem to work and I am not entirely sure why.

     */

    @Override
    public void onStop() {
        super.onStop();
        Log.d(this.getClass().toString(), "In on stop");
        if(imageHandler !=null) {
            imageHandler.killTask();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        passModel = (PassModel) context;
    }

    /*
        obtainScreenSize:   Gets the size of the screen in pixels so they can be used with the
                            image handler.
     */

    private void obtainScreenSize(){
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;

        screenHeight = metrics.heightPixels;

    }

    /*
        retrieveData:   Calls the class responsible for getting the json data from BreweryDB.
                        showing the progressbar and disposing of the image.
     */

    private void retrieveData(){
        imageView.setImageBitmap(null);
        progressBar.setVisibility(View.VISIBLE);

        dataRequest = new DataRequest(screenWidth, screenHeight);
        dataRequest.callRequest(requestListener);
    }

    /*
       DataRequestListener: Listener for the data and image setting up the layout items.
                            The functions are invoked when the DataRequest and ImageHandler classes
                            complete their tasks

     */


    DataRequestListener requestListener = new DataRequestListener() {


    /*
        OnDataReady: when data is available this will return it to the invoking class and flags to
                     confirm it came through ok.
        @param:
            pass: boolean to tell us if the request suceeded
            http_code: The code of the request to again see if it suceeded or failed
            model: the data model for the data.

     */

        @Override
        public void onDataReady(boolean pass,String code, Model model) {

            String description;

            if (pass && model != null) {

                Log.d(this.getClass().toString(), "imageHandler called");

                imageHandler  = new ImageHandler(requestListener, model.getData().getLabel().getLarge(), screenWidth, screenHeight );


                if (model.getData().getDescription() != null) {
                    description = model.getData().getDescription();
                    Log.d(this.getClass().toString(), description);
                } else if (model.getData().getStyle().getDescription() != null) {
                    description = "Sorry, there's no description of the beer available, here's the description of the style \n \n" + model.getData().getStyle().getDescription();
                    Log.d(this.getClass().toString(), description);
                } else {
                    description = "Sorry, there's no description of the beer or even the style of the beer.";
                    Log.d(this.getClass().toString(), "Category description is also empty, here's the id: " + model.getData().getId());
                }
                model.getData().setAbv("abv: " + model.getData().getAbv());

                model.getData().setDescription(description);
                tvTitle.setText(model.getData().getNameDisplay());
                tvAbv.setText(model.getData().getAbv());
                tvCategoryName.setText(model.getData().getStyle().getName());
                tvDescription.setText(model.getData().getDescription());

                passModel.passModel(model);

            }else if(!pass && code != null){
                Log.e(this.getClass().toString(), "Failed to pass result. Code: " + code);
            }else{
                Log.e(this.getClass().toString(), "General failure");
            }
        }

         /*
            imageReady: Invoked by the Imagehandler class when it has completed processing the bitmap.
                        Make progress bar invisible, sets the image and makes it visible.
                @Param:
                    bitmap: The now ready to use bitmap.
         */

        @Override
        public void imageReady(Bitmap bitmap) {
            if(bitmap !=null) {
                imageView.setImageBitmap(bitmap);
                progressBar.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.VISIBLE);
            }else{
                Log.e(this.getClass().toString(), "Bitmap was not returned.");
            }
        }
    };
}
