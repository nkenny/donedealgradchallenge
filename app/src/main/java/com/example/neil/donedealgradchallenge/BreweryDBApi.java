package com.example.neil.donedealgradchallenge;


import com.example.neil.donedealgradchallenge.model.simple.Model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Neil on 23/05/2017.
 *
 * Interface for dealing requests to the breweryDB api
 *
 */

public interface BreweryDBApi {

/*
Sample url:
    https://api.brewerydb.com/v2/beer/beer/random?hasLabels=Y&format=json&random?key=885e7ab87a61cca6e429ae096da18177

 */

    String BASE_URL = "http://api.brewerydb.com/v2/";
    String API_KEY = "885e7ab87a61cca6e429ae096da18177";

    /*
        GET Request for brewery DB API
        @param:
            api_key: key for brewery DB API, probably should have been hard coded here
     */
    @GET("beer/random?hasLabels=Y&format=json")
    Call<Model> getBeerData(@Query("key") String api_key);


}
