package com.example.neil.donedealgradchallenge.model.alldata;

/**
 * Created by Neil on 23/05/2017.
 *
 * Top level model for JSON data pulled from brewerydb api v2
 * Subset of data
 *
 * Sample data:
 *      message	"READ ONLY MODE: Request Successful"
 *      data
 *      status	"success"
 *
 */

public class Model {
    private String message;
    private String status;

    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
