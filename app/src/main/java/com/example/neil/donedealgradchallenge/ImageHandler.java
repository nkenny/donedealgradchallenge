package com.example.neil.donedealgradchallenge;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Neil on 25/05/2017.
 *
 *
 *  For calling and scaling images for the imageView.
 *
 *  Note:   There are libraries that will do the image handling better but I didn't think that was
 *          what was wanted.
 *
 *
 *  Bug:    Can't seem to get the thread to die when its scaling the image and the fragment it
 *          was scaling for stops.
 *
 *
 *
 */

public class ImageHandler {

    DataRequestListener dataRequestListener;

    private AsyncTask getImage;

    /*
        ImageHandler: Constructor, used to initiate the image and set the callback listener
                      and start the thread to get and scale the image

        @param:
            dataRequestListener: listener to call for invoking class to let it know that the image is ready.
            imageUrl: Url of the image.
            reqWidth: Required width of the image.
            reqHeight: Requiered height of the image.

     */

    public ImageHandler(DataRequestListener dataRequestListener, String imageUrl, int reqWidth, int reqHeight) {
        this.dataRequestListener = dataRequestListener;
        getImage = new GetImage().execute(imageUrl, Integer.toString(reqWidth), Integer.toString(reqHeight));
    }

    /*
        killTask: Used to cancel the invoked async task.
     */

    public void killTask(){
        Log.d(this.getClass().toString(), "In killtask");

        if(getImage.getStatus() == AsyncTask.Status.RUNNING || getImage.getStatus() == AsyncTask.Status.PENDING) {
            Log.d(this.getClass().toString(), "Canceling thread");
            getImage.cancel(true);
        }else{
            Log.d(this.getClass().toString(), "Thread already finished");
        }
    }

    /*
        GetImage:  Async task to get and scale image.
                    Async tasks are a requirement as pulling data from a stream in the UI thread
                    would lock up the UI thread.

        AsyncTask
            @param:
                Bitmap: BitmapObject sent to on post execute.

     */
    private class GetImage extends AsyncTask<String, Void, Bitmap> {


        /*
            doInBackground: Converts image Url to a url and sends it along side required width and
                            required height to the decodeBitmapFromStream

            @param:
                Params: Contains: Url, required Width, requiredHeight.
                return: Downloaded and scaled bitmap.

         */

        @Override
        protected Bitmap doInBackground(String... params) {

            try {

                Log.d(this.getClass().toString(), params[0]);
                URL imageUrl = new URL(params[0]);

                return decodeBitmapFromStream(imageUrl, Integer.parseInt(params[1]), Integer.parseInt(params[2]));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        /*
            onPostExecute:  sends the requested, scaled bitmap back to the calling class via the
                            listener
            @params:
                bitmap: bitmap decided in doInBackground.
         */

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            dataRequestListener.imageReady(bitmap);
        }
    }


    /*
        calculateSampleSize: Calculates the required sample size, sample size being the required
                             size of the image.(explanation of sample sizes below). It does this by
                             taking the image boundary and multiplying the sample size by 2 while
                             the original size halved divided by the sample size is bigger than the
                             required size.


                             A sample size returning a sample size of 1 would give us a full sized
                             image, 2 a half sized image, 4 a quarter sized image etc.

        @param:
            Options: Contains the width and height of the image to be scaled.
            reqWidth: Required width of the image
            reqHeight: Required height of the image.


     */

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight){

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if( height > reqHeight || width > reqWidth){

            final int halfHeight = height/2;
            final int halfWidth = width/2;

            while((halfHeight / inSampleSize) >= reqHeight &&(halfWidth / inSampleSize) >= reqWidth){
                inSampleSize *=2;
            }
        }

        return inSampleSize;
    }

    /*  decodeBitmapFromStream: Gets the width and height of the image to be scaled, calculates the
                                sampleSize and finally gets the image from the url scaling it as it
                                goes.

        @param:
            imageUrl: url of the image
            reqWidth: required width.
            reqHeight: required height.
        @return: decoded bitmap.

     */

    public Bitmap decodeBitmapFromStream(URL imageUrl, int reqWidth, int rewHeight) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream(), null, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, rewHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream(), null, options);

    }



}
