package com.example.neil.donedealgradchallenge;



import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.neil.donedealgradchallenge.model.simple.Model;

import java.util.ArrayList;

/*

    MainActivity: Used to; invoke and handle fragments, handle fragment data.


 */


public class MainActivity extends AppCompatActivity implements MainGuiFragment.PassModel {

    /*Holds the model data pulled by the fragments to allow us to go back and check the beer we've
    already see
    */
    private ArrayList<Model> modelFromFragment;
    /*Page limit for the screenslider pages.
      I've seen work arounds to make it infinite but they're a little dirty.

    */
    private static final int numberOfPages = 100;

    private ViewPager pager;
    private PagerAdapter pagerAdapter;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slider);

       /* MainGuiFragment fragment = new MainGuiFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();*/

        modelFromFragment = new ArrayList<>();

        pager = (ViewPager) findViewById(R.id.view_pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);


    }


    /*
        Pass model: Listener for when fragments complete their call, adds it to the model arraylist
                    to allow us to call on that data again later.

        @param
            model: Model that fragment just created.
     */
    @Override
    public void passModel(Model model) {
        Log.e(this.getClass().toString(), model.getData().getNameDisplay());
        modelFromFragment.add(model);

    }

    /*
        ScreenSliderPagerAdapter: Allows us to handle the slider via the FragmentStatePagerAdapter.



     */

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter{



        public ScreenSlidePagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        /*
            getItem:    Given current position of the slider either invokes a new fragment that calls
                        its own data or get the model from the modelFragmentArrayList and loads it
                        that way using bundles.
                @param:
                    position: Current position of the slider.

         */

        @Override
        public Fragment getItem(int position) {

            if(position < modelFromFragment.size() && !modelFromFragment.isEmpty()){
                Bundle bundle = new Bundle();
                bundle.putString("name", modelFromFragment.get(position).getData().getNameDisplay());
                bundle.putString("description", modelFromFragment.get(position).getData().getDescription());
                bundle.putString("abv", modelFromFragment.get(position).getData().getAbv());
                bundle.putString("category_name", modelFromFragment.get(position).getData().getStyle().getName());
                bundle.putString("label", modelFromFragment.get(position).getData().getLabel().getLarge());
                Fragment fragment = new MainGuiFragment();
                fragment.setArguments(bundle);
                return fragment;
            }else{
                return new MainGuiFragment();
            }

        }

        /*
            getCount: gets the number of pages the slider can have.

            @param:

            @return: number of pages it can have

         */

        @Override
        public int getCount() {
            return numberOfPages;
        }
    }








}
