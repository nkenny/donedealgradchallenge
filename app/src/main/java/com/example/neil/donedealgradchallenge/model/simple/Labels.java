package com.example.neil.donedealgradchallenge.model.simple;

/**
 * Created by Neil on 23/05/2017.
 */

public class Labels {

    private String icon = null;
    private String medium = null;
    private String large = null;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }
}
