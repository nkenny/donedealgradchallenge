package com.example.neil.donedealgradchallenge.model.simple;

/**
 * Created by Neil on 23/05/2017.
 */

public class Model {

    private String message = null;
    private String status = null;

    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
