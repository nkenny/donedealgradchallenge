package com.example.neil.donedealgradchallenge;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.neil.donedealgradchallenge.model.simple.Model;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Neil on 24/05/2017.
 */

public class DataRequest {

    private final String api_key ="885e7ab87a61cca6e429ae096da18177";

    DataRequestListener dataRequestListener;

    int screenWidth;
    int screenHeight;


    /*
        DataRequest: Constructor setting up the screen size to be passed to the imageHandler when called.

        @param:
            screenWidth: Width of device screen
            screenHeight: Height of device screen
     */
    DataRequest(int screenWidth, int screenHeight){
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    /*
        Call Request:   Calls the retrofit request to the BreweryDB api passing the results to
                        beerCallback.

        @param:
            requestListener:    Listener for the class that called this one to get the results and
                                set them to UI
     */
    public void callRequest(DataRequestListener requestListener){
        this.dataRequestListener = requestListener;

        Gson gson = new GsonBuilder().create();


        Retrofit retrofitBuilder = new Retrofit.Builder().baseUrl(BreweryDBApi.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();

        retrofitBuilder.create(BreweryDBApi.class).getBeerData(api_key).enqueue(breweryDBCallBack);

    }

    /*
       beerCallBack: Callback for the retrofit request called in callRequest.

     */
    Callback<Model> breweryDBCallBack = new Callback<Model>() {

        /*
            onResponse: returns response from the call from retrofit to breweryDB.
            @param:
                call: The request
                response: The response from the request including header files, json data etc.

         */

        @Override
        public void onResponse(Call<Model> call, Response<Model> response) {

            if (response.isSuccessful() && response.body().getStatus().equals("success")) {
                Log.d(this.getClass().toString(), response.toString() + response.body().getData().getLabel().getMedium());
                dataRequestListener.onDataReady(true, Integer.toString(response.code()), response.body());



            } else {
                Log.e(this.getClass().toString(), "HTTP code: " + Integer.toString(response.code()) + "\t Status from json: " + response.body().getStatus());
                dataRequestListener.onDataReady(false, Integer.toString(response.code()), null);

            }


        }

        /*
            OnFailure; In case the request fails due to network error or a failure creating the
                       response.

         */

        @Override
        public void onFailure(Call<Model> call, Throwable t) {

            dataRequestListener.onDataReady(false,"General failure in onFailure", null);

            t.printStackTrace();
        }
    };




}
