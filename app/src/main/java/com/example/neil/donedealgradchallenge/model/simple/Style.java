package com.example.neil.donedealgradchallenge.model.simple;

/**
 * Created by Neil on 23/05/2017.
 */

public class Style {

    private String name = null;
    private String description = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
