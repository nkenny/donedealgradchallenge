package com.example.neil.donedealgradchallenge.model.simple;

/**
 * Created by Neil on 23/05/2017.
 */

public class Data {

    private String id = null;
    private String nameDisplay = null;
    private String description = null;
    private String abv = "";
    private String status = null;

    private Labels labels;
    private Style style;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameDisplay() {
        return nameDisplay;
    }

    public void setNameDisplay(String nameDisplay) {
        this.nameDisplay = nameDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbv() {
        return abv;
    }

    public void setAbv(String abv) {
        this.abv = abv;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Labels getLabel() {
        return labels;
    }

    public void setLabel(Labels label) {
        this.labels = label;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }
}
