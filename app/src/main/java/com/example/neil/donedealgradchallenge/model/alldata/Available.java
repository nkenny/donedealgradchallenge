package com.example.neil.donedealgradchallenge.model.alldata;

/**
 * Created by Neil on 23/05/2017.
 *
 * Availablilty data
 * Subset of data
 *
 * Sample data:
 *       id	    4
 *       name	"Seasonal"
 *       description	"Available at the same time of year, every year."
 *
 */

public class Available {

    private int id;
    private String name;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
