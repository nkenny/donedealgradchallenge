package com.example.neil.donedealgradchallenge.model.alldata;

/**
 * Created by Neil on 23/05/2017.
 *
 *  Data layer from json request
 *  Subset of model
 *
 *  Sample data:
 *
 *      id	"vZBJHc"
 *      name	"Peacemaker"
 *      nameDisplay	"Peacemaker"
 *      description	"Peace is a dangerous thing.  Peacemaker is a west coast style American Pale Ale that uses several hop varieties to produces a unique and bountiful hop aroma with well balanced bitterness.  A special blend of American and European malts make this a very well rounded, characterful beer"
 *      abv	"5.7"
 *      glasswareId	5
 *      availableId	4
 *      styleId	25
 *      isOrganic	"N"
 *      labels	Object
 *      status	"verified"
 *      statusDisplay	"Verified"
 *      createDate	"2012-01-03 02:43:55"
 *      updateDate	"2016-07-20 20:43:01"
 *      glass	Object
 *      available	Object
 *      style	Object
 *
 */

public class Data {

    private String id;
    private String name;
    private String nameDisplay;
    private String description;
    private String abv;
    private int glasswareId;
    private int availableId;
    private int styleId;
    private char isOrganic;
    private String status;
    private String statusDisplay;
    private String createDate;
    private String updateDate;


    private Labels labels;
    private Glass glass;
    private Available available;
    private Style style;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameDisplay() {
        return nameDisplay;
    }

    public void setNameDisplay(String nameDisplay) {
        this.nameDisplay = nameDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbv() {
        return abv;
    }

    public void setAbv(String abv) {
        this.abv = abv;
    }

    public int getGlasswareId() {
        return glasswareId;
    }

    public void setGlasswareId(int glasswareId) {
        this.glasswareId = glasswareId;
    }

    public int getAvailableId() {
        return availableId;
    }

    public void setAvailableId(int availableId) {
        this.availableId = availableId;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public char getIsOrganic() {
        return isOrganic;
    }

    public void setIsOrganic(char isOrganic) {
        this.isOrganic = isOrganic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDisplay() {
        return statusDisplay;
    }

    public void setStatusDisplay(String statusDisplay) {
        this.statusDisplay = statusDisplay;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Labels getLabels() {
        return labels;
    }

    public void setLabels(Labels labels) {
        this.labels = labels;
    }

    public Glass getGlass() {
        return glass;
    }

    public void setGlass(Glass glass) {
        this.glass = glass;
    }

    public Available getAvailable() {
        return available;
    }

    public void setAvailable(Available available) {
        this.available = available;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }
}
