package com.example.neil.donedealgradchallenge.model.alldata;

/**
 * Created by Neil on 23/05/2017.
 *
 * Category details
 * Subset of style
 *
 * Sample data:
 *      id	3
 *      name	"North American Origin Ales"
 *      createDate	"2012-03-21 20:06:45"
 *
 */

public class Category {
    private int id;
    private String name;
    private String createDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
