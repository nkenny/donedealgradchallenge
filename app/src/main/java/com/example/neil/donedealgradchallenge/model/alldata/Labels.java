package com.example.neil.donedealgradchallenge.model.alldata;

/**
 * Created by Neil on 23/05/2017.
 *
 * Label urls (end in .png)
 * Subset of data
 *
 * Sample data:
 *      icon	"https://s3.amazonaws.com/brewerydbapi/beer/vZBJHc/upload_81GeIt-icon.png"
 *      medium	"https://s3.amazonaws.com/…upload_81GeIt-medium.png"
 *      large	"https://s3.amazonaws.com/…/upload_81GeIt-large.png"
 *
 *
 */

public class Labels {

    private String icon;
    private String medium;
    private String large;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }
}
