package com.example.neil.donedealgradchallenge.model.alldata;

/**
 * Created by Neil on 23/05/2017.
 *
 * Glass type data
 * Subset of data
 *
 * Sample data
 *
 *      id	5
 *      name	"Pint"
 *      createDate	"2012-01-03 02:41:33"
 *
 */

public class Glass {

    private int id;
    private String name;
    private String createDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
