Graduate developer challenge for the Donedeal, objective of the application was to create a single screen that pulled and displayed data from the BreweryDB api. using retrofit. 

- Usable from version 21 to above. 
- Uses retrofit. 
- Implemented some aspects of material design. I.e. Collapsing tool bar and slider.
- built with gradle.


The main thing I would change is I couldn't think of a nice way to show the users you can swipe right to check out new beers and left to check out beers you've already seen.